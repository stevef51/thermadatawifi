#!/bin/bash
APP=$0
SERIALNUMBER=$1
shift
READKEY=$1
shift

function usage {
    echo "$APP <serialnumber> <readkey> [--dumpSoap]"
    exit
}

if [ -z "$SERIALNUMBER" ] || [ -z "$READKEY" ]; then 
    usage
fi

echo SERIALNUMBER=$SERIALNUMBER
echo READKEY=$READKEY

echo CheckClientAuthentication $@
./raw.sh CheckClientAuthentication $@

echo RequestReadAccessToInstrument2 $SERIALNUMBER $READKEY $@
./raw.sh RequestReadAccessToInstrument2 $SERIALNUMBER $READKEY $@

echo GetMyConnectionsEx2 $@
./raw.sh GetMyConnectionsEx2 $@

./getpackets.sh $@