const mongodb = require('mongodb');
const ObjectId = mongodb.ObjectId;
const config = require('../config');
const url = config.mongoDb.url;
const _ = require('lodash');

console.log(`MongoDb ${config.mongoDb.url} database ${config.mongoDb.dbName}`);

module.exports = async function () {
    const client = await mongodb.MongoClient.connect(config.mongoDb.url, { useNewUrlParser: true });
    const db = client.db(config.mongoDb.dbName);
    const masterCollection = db.collection(`master`);

    async function getMasterConfig() {
        return masterCollection.findOne({
            _id: `master-config`
        })
    }

    async function getTenantDb(tenant) {
        const safeTenantName = tenant.replace(/\./g, '-');
        const tenantCollection = db.collection(`${safeTenantName}`);
        const indexes = [{ type: 1 }, { serialNumber: 1 }, { dateTime: 1 }];

        const TENANT_CONFIG = `tenant-config`;
        await Promise.all(indexes.map(i => tenantCollection.createIndex(i)));

        async function getTenantConfig() {
            return tenantCollection.findOne({
                _id: TENANT_CONFIG
            })
        }

        async function updateTenantConfig(config) {
            return (await tenantCollection.findOneAndReplace({ _id: TENANT_CONFIG }, Object.assign({
                _id: TENANT_CONFIG,
                type: TENANT_CONFIG
            }, config), {
                upsert: true
            })).value;
        }

        async function addReadings(readings) {
            let readingsWithId = readings.map(r => {
                r._id = `es-reading/${r.serialNumber}/${r.sensor}/${r.dateTime.toISOString()}`;
                r.type = `es-reading`;
                return r;
            })
            try {
                let result = await tenantCollection.insertMany(readingsWithId, { ordered: false });
                return {
                    inserted: result.insertedCount
                }
            } catch (e) {
                if (e.code === 11000) {     // Duplicate key error collection
                    return {
                        inserted: e.result.nInserted,
                        duplicate: e.writeErrors.length
                    }
                }
            }
        }

        async function addProbeDatas(readings) {
            let readingsWithId = readings.map(r => {
                r._id = `es-probe-data/${r.serialNumber}/${r.dateTime.toISOString()}`;
                r.type = `es-probe-data`;
                return r;
            })
            try {
                let result = await tenantCollection.insertMany(readingsWithId, { ordered: false });
                return {
                    inserted: result.insertedCount
                }
            } catch (e) {
                if (e.code === 11000) {     // Duplicate key error collection
                    return {
                        inserted: e.result.nInserted,
                        duplicate: e.writeErrors.length
                    }
                }
            }
        }

        return {
            getTenantConfig,
            updateTenantConfig,
            addReadings,
            addProbeDatas
        }
    }

    async function close() {
        return client.close();
    }

    return {
        getMasterConfig,
        getTenantDb,
        close
    }
}