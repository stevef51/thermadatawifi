module.exports = async function () {
    let connection = {};
    connection.id = require('uuid/v4')();

    const client = await require('./client')();
    try {
        var connectionResult = await client.CreateNewConnectionAsync({ connectionID: connection.id });
        connection.secret = connectionResult[0].CreateNewConnectionResult;
    } catch (e) {
        console.error(`${e.Fault.statusCode} ${e.body}`);
        throw e;
    }
    return connection;
}
