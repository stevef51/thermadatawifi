
const signature = require('./signature');

function fn(client, connection, check) {
    const GetCachedInstrumentInfoAsync = async args => {
        return await client.GetCachedInstrumentInfoAsync({
            connectionID: connection.id,
            serialNumber: args.serialNumber,
            authenticate: signature.generate(connection, [
                signature.fromString(args.serialNumber)
            ])
        }).then(check);
    }
    GetCachedInstrumentInfoAsync.params = [`serialNumber`];
    return GetCachedInstrumentInfoAsync;
}

module.exports = fn;
