
const signature = require('./signature');

function fn(client, connection, check) {
    const RequestRetrieveInstrumentHistory2Async = async args => {
        return await client.RequestRetrieveInstrumentHistory2Async({
            connectionID: connection.id,
            instrumentSerialNumber: args.serialNumber,
            requestID: args.requestId,
            authenticate: signature.generate(connection, [
                signature.fromString(args.serialNumber),
                signature.fromGuid(args.requestId)
            ])
        }).then(check).then(response => {
            if (response.Error === 'NoError') {
                response.requestId = args.requestId;
            }
            return response;
        });
    }
    RequestRetrieveInstrumentHistory2Async.params = [`serialNumber`, `requestId`]
    return RequestRetrieveInstrumentHistory2Async;
}

module.exports = fn;
