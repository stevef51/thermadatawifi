const uuidParse = require('uuid-parse');
const Buffer = require('safe-buffer').Buffer;
const crypto = require('crypto');

function generate(connection, args) {
    let bytes = Buffer.concat([
        fromGuid(connection.id),
        fromGuid(connection.secret),
        ...args]);

    const hash = crypto.createHash('sha512');
    hash.update(bytes);
    let digest = hash.digest();

    return digest.toString('base64');
}

function fromBool(value) {
    return Buffer.from([!!value]);
}

function fromGuid(value) {
    let bytes = Buffer.alloc(16);
    uuidParse.parse(value, bytes, 0);
    // Little endian according to https://docs.microsoft.com/en-us/dotnet/api/system.guid.tobytearray?redirectedfrom=MSDN&view=netframework-4.8#System_Guid_ToByteArray
    bytes = Buffer.concat([
        bytes.slice(0, 4).swap32(),
        bytes.slice(4, 6).swap16(),
        bytes.slice(6, 8).swap16(),
        bytes.slice(8)]);
    return bytes;
}

function fromInt32(value) {
    let buf = Buffer.alloc(4);
    buf.writeInt32LE(value, 0);
    return buf;
}

function fromString(value) {
    return Buffer.from(value, 'utf8');
}

module.exports = {
    generate,
    fromBool,
    fromGuid,
    fromInt32,
    fromString
}