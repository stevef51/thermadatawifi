
const signature = require('./signature');

function fn(client, connection, check) {
    const RemoveAccessToLoggerAsync = async args => {
        return await client.RemoveAccessToLoggerAsync({
            connectionID: connection.id,
            serialNumber: args.serialNumber,
            authenticate: signature.generate(connection, [
                signature.fromString(args.serialNumber)
            ])
        }).then(check);
    }
    RemoveAccessToLoggerAsync.params = [`serialNumber`];
    return RemoveAccessToLoggerAsync;
}

module.exports = fn;