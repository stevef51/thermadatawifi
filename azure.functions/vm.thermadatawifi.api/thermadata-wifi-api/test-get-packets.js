
const signature = require('./signature');

function fn(client, connection, check) {
    const TestGetPacketsAsync = async (args) => {
        const maxConnection = Number(args.maxConnection || 30);
        return await client.TestGetPacketsAsync({
            connectionID: connection.id,
            maxConnection: maxConnection,
            // Inconsistent API naming ..
            authentication: signature.generate(connection, [
                signature.fromInt32(maxConnection)
            ])
        }).then(check);
    }
    TestGetPacketsAsync.params = [
        'maxConnection'
    ]

    return TestGetPacketsAsync;
}

module.exports = fn;
