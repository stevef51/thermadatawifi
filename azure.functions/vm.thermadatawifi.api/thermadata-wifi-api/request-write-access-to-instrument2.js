
const signature = require('./signature');

function fn(client, connection, check) {
    const RequestWriteAccessToInstrument2Async = async (args) => {
        return await client.RequestWriteAccessToInstrument2Async({
            connectionID: connection.id,
            serialNumber: args.serialNumber,
            writeKey: args.writeKey,
            authenticate: signature.generate(connection, [
                signature.fromString(args.serialNumber),
                signature.fromString(args.writeKey)
            ])
        }).then(check);
    }
    RequestWriteAccessToInstrument2Async.params = [
        'serialNumber',
        'writeKey',
    ]
    return RequestWriteAccessToInstrument2Async;
}

module.exports = fn;
