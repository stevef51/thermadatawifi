/*
Extract the actual result of the SOAP call, the Client SOAP methods return data of the following form
 
[{ response }, <soapRequest>, <soapResponse>]
 
we are not interested in the soapRequest and soapResonse, the SOAP Client parses these out for us
the { response } object is of the form
 
{ "methodName"Result: <result> }
 
so we just want to return <result> 
 
*/
module.exports = function (dumpSoapXml) {
    return async function check(result) {
        let objResult = result;
        if (typeof objResult.length !== `undefined`) {
            if (dumpSoapXml) {
                console.error(`${result[3]}\n`);
                console.error(`${result[1]}\n`);
            }
            objResult = objResult[0];
            if (objResult == null) {
                return objResult;
            }
        }
        // Grab the 1st property name of the { response }
        let resultProp = Object.keys(objResult)[0];

        // and return its value
        return objResult[resultProp];
    }
}