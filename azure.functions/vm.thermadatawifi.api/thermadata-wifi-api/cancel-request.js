
const signature = require('./signature');

function fn(client, connection, check) {
    const CancelRequestAsync = async (args) => {
        return await client.CancelRequestAsync({
            connectionID: connection.id,
            serialNumber: args.serialNumber,
            requestID: args.requestId,
            // Inconsistent API naming ..
            authentication: signature.generate(connection, [
                signature.fromString(args.serialNumber),
                signature.fromGuid(args.requestId)
            ])
        }).then(check);
    }
    CancelRequestAsync.params = [`serialNumber`, `requestId`]
    return CancelRequestAsync;
}

module.exports = fn;