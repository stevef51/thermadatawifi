
const signature = require('./signature');

function fn(client, connection, check) {
    const GetLatestInstrumentFirmwareVersionAsync = async args => {
        return await client.GetLatestInstrumentFirmwareVersionAsync().then(check);
    }
    GetLatestInstrumentFirmwareVersionAsync.params = []

    return GetLatestInstrumentFirmwareVersionAsync;
}

module.exports = fn;
