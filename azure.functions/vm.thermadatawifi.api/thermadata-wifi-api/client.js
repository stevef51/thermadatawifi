const soap = require('soap');
const wsdlUrl = `https://thermadatawifi.trafficmanager.net/externalapp.asmx?wsdl`;

let _client;

async function getClient() {
    if (!_client) {
        _client = await soap.createClientAsync(wsdlUrl);
    }
    return _client;
}

module.exports = getClient;

