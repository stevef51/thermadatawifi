
const signature = require('./signature');

function fn(client, connection, check) {
    const RequestFullInstrumentInformation2Async = async args => {
        return await client.RequestFullInstrumentInformation2Async({
            ConnectionID: connection.id,         // Inconsistent API naming casing
            serialNumber: args.serialNumber,
            requestID: args.requestId,
            authenticate: signature.generate(connection, [
                signature.fromString(args.serialNumber),
                signature.fromGuid(args.requestId)
            ])
        }).then(check);
    }
    RequestFullInstrumentInformation2Async.params = [`serialNumber`, `requestId`]
    return RequestFullInstrumentInformation2Async;
}

module.exports = fn;
