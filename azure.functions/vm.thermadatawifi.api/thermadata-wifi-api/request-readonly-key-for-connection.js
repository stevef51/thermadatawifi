
const signature = require('./signature');

function fn(client, connection, check) {
    const RequestReadOnlyKeyForConnectionAsync = async args => {
        return await client.RemoveAccessToLoggerAsync({
            connectionID: connection.id,
            authenticate: signature.generate(connection, [
            ])
        }).then(check);
    }
    RequestReadOnlyKeyForConnectionAsync.params = [];
    return RequestReadOnlyKeyForConnectionAsync;
}

module.exports = fn;
