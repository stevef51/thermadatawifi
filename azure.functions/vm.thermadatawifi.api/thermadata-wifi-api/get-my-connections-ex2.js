
const signature = require('./signature');

function fn(client, connection, check) {
    const GetMyConnectionsEx2Async = async () => {
        return await client.GetMyConnectionsEx2Async({
            connectionID: connection.id,
            // Inconsistent API naming ..
            authentication: signature.generate(connection, [])
        }).then(check);
    }
    GetMyConnectionsEx2Async.params = [];
    return GetMyConnectionsEx2Async;
}

module.exports = fn;
