const signature = require('./signature');
const Buffer = require('safe-buffer').Buffer;
const _ = require('lodash');
const ByteStream = require('../utils/byte-stream');
const uuidParse = require('uuid-parse');

function GUID(s) {
    let b = s.UInt8(16);
    this.guid = uuidParse.unparse(b);
}

// See "ThermaData Wifi endpoint protocol - R13.pdf" for details on how these structures are encoded
function TU(s) {
    // 24 bit word split
    // 0..3 = units
    // 4..23 = (measurement + 400) * 300
    //                                          [0...23] b[ 0  1  2]
    // measurement of 0 = 400 * 300 = 120000 = 0x01D4C0 -> C0 D4 u1
    let b = s.UInt8(3);
    let i24 = b[0] + (b[1] << 8) + (b[2] << 16);    // read it as a 24 bit LE word
    let units = (i24 & 0x00F00000) >>> 20;
    let value = (i24 & 0x000FFFFF);
    this.units = units;
    switch (value) {
        case 0xFFFFF:
            this.error = true;
            break;
        case 0xFFFFE:
            this.auditCheck = true;
            break;
        case 0xFFFFD:
            this.noReadingYet = true;
            break;
        default:
            this.measurement = (value / 300.0) - 400.0;
            break;
    }
}

function TRIM(s) {
    // 16 bit word split
    // 0..3 = units
    // 4..15 = (trim + 6) * 300

    let b = s.UInt8(2);
    let i16 = b[0] + (b[1] << 8);
    let units = (i16 & 0xF000) >>> 12;
    let value = (i16 & 0x0FFF);
    this.trim = (value / 300.0) - 6.0;
}

function Date_Packet(s) {
    let b = s.UInt8(2);
    let i16 = b[1] + (b[0] << 8);
    this.day = ((i16 & 0x1F00) >>> 8);
    this.month = ((i16 & 0xE000) >>> 13) + ((i16 & 0x0001) << 4);
    this.year = 2015 + ((i16 & 0x007E) >>> 1);
}

function DateTimePackedForSavedReading(s) {
    let b = s.UInt32LE();
    this.hour = (b & 0xF8000000) >>> 27;
    this.minute = (b & 0x07E00000) >>> 21;
    this.second = (b & 0x001F8000) >>> 15;
    this.year = 2015 + ((b & 0x00007E00) >>> 9);
    this.month = (b & 0x000001E0) >>> 5;
    this.day = (b & 0x0000001F);
}

const _UnitType = {
    0: `Unknown`,
    1: `C`,
    2: `F`,
    3: `Humidity`
}

function SAVEDREADING(s) {
    let t = new TU(s);
    this.sensor = t.units >= 8 ? 2 : 1;
    this.unit = _UnitType[t.units & 0x3];
    if (t.auditCheck)
        this.auditCheck = t.auditCheck;
    if (t.error)
        this.error = t.error;
    if (t.noReadingYet)
        this.noReadingYet = t.noReadingYet;
    if (t.measurement != null)
        this.measurement = t.measurement;
    this.dateTime = new DateTimePackedForSavedReading(s);
}

function BroadcastReadingPacket(s) {
    this.type = "BroadcastReadingPacket";
    this.numReadings = s.UInt16LE();
    this.sensor1Maximum = new SAVEDREADING(s);
    this.sensor1Minimum = new SAVEDREADING(s);
    this.sensor2Maximum = new SAVEDREADING(s);
    this.sensor2Minimum = new SAVEDREADING(s);
    this.sensorTypes = new SensorTypes(s);;
    this.signalStrength = new SignalStrength(s);
    this.batteryLevel = new BatteryLevel(s);
    this.instrumentStatus = new InstrumentStatus(s);
    this.configuration = s.UInt8();
    this.sensor1HighAlarmCount = s.UInt32LE();
    this.sensor1LowAlarmCount = s.UInt32LE();
    this.sensor2HighAlarmCount = s.UInt32LE();
    this.sensor2LowAlarmCount = s.UInt32LE();
    this.totalReadingsSinceStart = s.UInt32LE();

    this.readings = [];
    for (let i = 0; i < this.numReadings; i++) {
        this.readings.push(new SAVEDREADING(s));
    }
}

function SavedReadingsPacket(s) {
    this.type = "SavedReadingsPacket";
    this.indexOfFirstReading = s.UInt16LE();
    this.numOfReadingsValid = s.UInt16LE();
    this.requestId = new GUID(s);
    this.readings = [];
    for (let i = 0; i < this.numOfReadingsValid; i++) {
        let r = new SAVEDREADING(s);
        r.historyIndex = this.indexOfFirstReading + i;
        this.readings.push(r);
    }
}

function BatteryLevel(s) {
    this.value = s.UInt8();
    this.isLow = this.value <= 157;
}

function SignalStrength(s) {
    this.dbm = s.UInt8() - 100;
}

const _SensorTypes = {
    0: `Unknown`,
    1: `not present`,
    2: `Internal Thermistor`,
    3: `External Thermistor`,
    4: `Humidity Temperature Sensor`,
    5: `Humidity Sensor`,
    6: `Thermocouple Sensor K`,
    7: `Thermocouple Sensor T`,
    8: `PT1000/PT100 Sensor`,
    9: `Infrared`
}

function SensorTypes(s) {
    this.value = s.UInt8();
    this.sensor1 = _SensorTypes[this.value >>> 4];
    this.sensor2 = _SensorTypes[this.value & 0x0F];
}

function InstrumentSettings(s) {
    this.value = s.UInt8();
    this.sensor1AlarmHold = !!(this.value & 0x01);
    this.sensor1Disabled = !!(this.value & 0x02);
    this.sensor2Disabled = !!(this.value & 0x04);
    this.displayUnits = (this.value & 0x08) ? "F" : "C";
    this.sensor1HighAlarmEnabled = !!(this.value & 0x10);
    this.sensor2HighAlarmEnabled = !!(this.value & 0x20);
    this.sensor1LowAlarmEnabled = !!(this.value & 0x40);
    this.sensor2LowAlarmEnabled = !!(this.value & 0x80);
}

function InstrumentSettings2(s) {
    this.value = s.UInt8();
    this.sensor2AlarmHold = !!(this.value & 0x01);
    this.auditEnabled = !!(this.value & 0x02);
}

const _InstrumentStatus = {
    0: `Unknown`,
    1: `Waiting to Start Delayed`,
    2: `Waiting to Start Timed Start`,
    3: `Waiting to Start Manual`,
    0x10: `Logging`,
    0x20: `Stopped Manually`,
    0x21: `Stopped at Timed End`,
    0x22: `Stopped as Full`,
    0x23: `Stopped at Requested Quantity`,
    0x24: `Stopped because of Low Battery`
}

function InstrumentStatus(s) {
    this.value = s.UInt8();
    this.text = _InstrumentStatus[this.value];
}

function AcknowledgementPacket(s) {
    this.type = "AcknowledgementPacket";
    this.packetBeingAcknowledged = s.UInt8();
    this.requestId = new GUID(s);
}

function InstrumentInformationPacket(s) {
    this.type = "InstrumentInformationPacket";
    this.sensorTypes = new SensorTypes(s);
    this.instrumentSettings = new InstrumentSettings(s);
    this.instrumentSettings2 = new InstrumentSettings2(s);
    this.batteryLevel = new BatteryLevel(s);
    this.firmwareVersion = s.UInt8(4);
    this.wifiModuleNWPVersion = s.UInt32LE(4);
    this.timeZoneOffset = s.UInt8();
    this.instrumentCalibrationDate = new Date_Packet(s);
    this.sensor1Name = s.UTF8String(32);
    this.sensor1HighLimit = new TU(s);
    this.sensor1LowLimit = new TU(s);
    this.sensor1Trim = new TRIM(s);
    this.sensor1TrimSetAt = new Date_Packet(s);
    this.sensor1LastReading = new SAVEDREADING(s);
    this.sensor1Maximum = new SAVEDREADING(s);
    this.sensor1Minimum = new SAVEDREADING(s);
    this.sensor1HighAlarmCount = s.UInt32LE();
    this.sensor1LowAlarmCount = s.UInt32LE();
    this.sensor1ThisTimeAlarmDelay = s.UInt16LE();
    this.sensor1ThisLogAlarmDelay = s.UInt16LE();
    this.sensor2Name = s.UTF8String(32);
    this.sensor2HighLimit = new TU(s);
    this.sensor2LowLimit = new TU(s);
    this.sensor2Trim = new TRIM(s);
    this.sensor2TrimSetAt = new Date_Packet(s);
    this.sensor2LastReading = new SAVEDREADING(s);
    this.sensor2Maximum = new SAVEDREADING(s);
    this.sensor2Minimum = new SAVEDREADING(s);
    this.sensor2HighAlarmCount = s.UInt32LE();
    this.sensor2LowAlarmCount = s.UInt32LE();
    this.sensor2ThisTimeAlarmDelay = s.UInt16LE();
    this.sensor2ThisLogAlarmDelay = s.UInt16LE();
    this.trimPassword = s.UTF8String(9);
    this.logIntervalInSeconds = s.UInt16LE();
    this.logStartedAt = new DateTimePackedForSavedReading(s);
    this.instrumentStatus = new InstrumentStatus(s);
    this.totalNumberOfReadingsStored = s.UInt32LE();
    this.calibrationInformation = s.UInt8(34);
    this.signalStrength = new SignalStrength(s);
    this.earliestReadingStored = new DateTimePackedForSavedReading(s);
    this.latestReadingStored = new DateTimePackedForSavedReading(s);
    this.transmitIntervalInSeconds = s.UInt16LE();
    this.requestId = new GUID(s);
    this.startStopMode = s.UInt8();
}

function UpdatedSSIDForInstrumentPacket(s) {
    this.type = 'UpdatedSSIDForInstrumentPacket';
    this.requestId = new GUID(s);
    this.SSID = s.UTF8String(32);
}

function consume(s) {
    let pktType = s.UInt8();
    switch (pktType) {
        case 1:
            return new InstrumentInformationPacket(s);

        case 3:
            return new BroadcastReadingPacket(s);

        case 4:
            return new SavedReadingsPacket(s);

        case 11:
            return new AcknowledgementPacket(s);

        case 20:
            return new UpdatedSSIDForInstrumentPacket(s);

        default:
            throw new Error(`Unknown packet type ${pktType}`);
    }
}

function fn(client, connection, check) {
    const GetPackets2Async = async (args) => {
        const maxConnection = Number(args.maxConnection || 30);
        return await client.GetPackets2Async({
            connectionID: connection.id,
            maxConnection: maxConnection,
            // Inconsistent API naming ..
            authenticate: signature.generate(connection, [
                signature.fromInt32(maxConnection)
            ])
        }).then(check).then(response => {
            if (response.Error === `NoError` && response.Packets != null) {
                response.Polls = _.chain(response.Packets.base64Binary)
                    .map(b64 => Buffer.from(b64, 'base64'))
                    .map(buf => {
                        let s = new ByteStream(buf);
                        let serialNumber = s.UInt8(4).toString('hex');
                        let packets = [];
                        while (s.more()) {
                            packets.push(consume(s));
                        }
                        return {
                            serialNumber, packets
                        };
                    })
                    .value();
                delete response.Packets;
            }
            return response;
        })
    }
    GetPackets2Async.params = [
        'maxConnection'
    ]
    return GetPackets2Async;
}

module.exports = fn;
