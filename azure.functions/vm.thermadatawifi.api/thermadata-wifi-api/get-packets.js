
const signature = require('./signature');

function fn(client, connection, check) {
    const GetPacketsAsync = async (args) => {
        const maxConnection = Number(args.maxConnection || 30);
        return await client.GetPacketsAsync({
            connectionID: connection.id,
            maxConnection: maxConnection,
            // Inconsistent API naming ..
            authentication: signature.generate(connection, [
                signature.fromInt32(maxConnection)
            ])
        }).then(check);
    }
    GetPacketsAsync.params = [
        'maxConnection'
    ]
    return GetPacketsAsync;
}

module.exports = fn;
