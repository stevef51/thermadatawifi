
const signature = require('./signature');

function fn(client, connection, check) {
    const GetCachedInstrumentInfoReadOnlyAsync = async args => {
        return await client.GetCachedInstrumentInfoReadOnlyAsync({
            connectionID: connection.id,
            serialNumber: args.serialNumber,
            authenticate: signature.generate(connection, [
                signature.fromString(args.serialNumber)
            ])
        }).then(check);
    }
    GetCachedInstrumentInfoReadOnlyAsync.params = [`serialNumber`];
    return GetCachedInstrumentInfoReadOnlyAsync;
}

module.exports = fn;
