
const signature = require('./signature');

function fn(client, connection, check) {
    const SetInstrumentNameAsync = async args => {
        return await client.SetInstrumentNameAsync({
            connectionID: connection.id,
            serialNumber: args.serialNumber,
            newName: args.newName,
            authenticate: signature.generate(connection, [
                signature.fromString(args.serialNumber),
                signature.fromString(args.newName)
            ])
        }).then(check);
    }
    SetInstrumentNameAsync.params = [`serialNumber`, `newName`]
    return SetInstrumentNameAsync;
}

module.exports = fn;
