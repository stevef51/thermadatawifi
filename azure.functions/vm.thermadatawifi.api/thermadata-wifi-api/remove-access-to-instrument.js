
const signature = require('./signature');

function fn(client, connection, check) {
    const RemoveAccessToInstrumentAsync = async (args) => {
        return await client.RemoveAccessToInstrumentAsync({
            connectionID: connection.id,
            serialNumber: args.serialNumber,
            authenticate: signature.generate(connection, [
                signature.fromString(args.serialNumber)
            ])
        }).then(check)  // Note, this always seems to return True
    }
    RemoveAccessToInstrumentAsync.params = [
        'serialNumber'
    ]
    return RemoveAccessToInstrumentAsync;
}

module.exports = fn;
