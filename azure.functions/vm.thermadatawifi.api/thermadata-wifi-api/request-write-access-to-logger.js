
const signature = require('./signature');

function fn(client, connection, check) {
    const RequestWriteAccessToLoggerAsync = async args => {
        return await client.RequestWriteAccessToLoggerAsync({
            connectionID: connection.id,
            serialNumber: args.serialNumber,
            writeKey: args.writeKey,
            authenticate: signature.generate(connection, [
                signature.fromString(args.serialNumber),
                signature.fromString(args.writeKey)
            ])
        }).then(check);
    }
    RequestWriteAccessToLoggerAsync.params = [`serialNumber`, `writeKey`];
    return RequestWriteAccessToLoggerAsync;
}

module.exports = fn;
