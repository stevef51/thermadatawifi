
const signature = require('./signature');

function fn(client, connection, check) {
    const RequestReadAccessToInstrument2Async = async (args) => {
        return await client.RequestReadAccessToInstrument2Async({
            connectionID: connection.id,
            serialNumber: args.serialNumber,
            readKey: args.readKey,
            authenticate: signature.generate(connection, [
                signature.fromString(args.serialNumber),
                signature.fromString(args.readKey)
            ])
        }).then(check).then(response => {
            return response === 'Success';          // Seems to return "Success" if it was added or "Failed" if it was already added
        });
    }
    RequestReadAccessToInstrument2Async.params = [
        'serialNumber',
        'readKey',
    ]
    return RequestReadAccessToInstrument2Async;
}

module.exports = fn;
