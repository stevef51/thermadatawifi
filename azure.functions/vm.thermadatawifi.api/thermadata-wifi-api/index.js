const CancelRequestAsync = require('./cancel-request');
const CheckClientAuthenticationAsync = require('./check-client-authentication');
const GetCachedInstrumentInfoReadOnlyAsync = require('./get-cached-instrument-info-readonly');
const GetCachedInstrumentInfoAsync = require('./get-cached-instrument-info');
const GetCachedLoggerInfoAsync = require('./get-cached-logger-info');
const GetLatestInstrumentFirmwareVersionAsync = require('./get-latest-instrument-firmware-version');
const GetMyConnectionsEx2Async = require('./get-my-connections-ex2');
const GetPacketsAsync = require('./get-packets');
const GetPackets2Async = require('./get-packets2');
const RemoveAccessToInstrumentAsync = require('./remove-access-to-instrument');
const RemoveAccessToLoggerAsync = require('./remove-access-to-logger');
const RequestFullInstrumentInformation2Async = require('./request-full-instrument-information2');
const RequestReadAccessToInstrument2Async = require('./request-read-access-to-instrument2');
const RequestReadAccessToLoggerAsync = require('./request-read-access-to-logger');
const RequestReadOnlyKeyForConnectionAsync = require('./request-readonly-key-for-connection');
const RequestRetrieveInstrumentHistory2Async = require('./request-retrieve-instrument-history2');
const RequestWriteAccessToInstrument2Async = require('./request-write-access-to-instrument2');
const RequestWriteAccessToLoggerAsync = require('./request-write-access-to-logger');
const SetInstrumentNameAsync = require('./set-instrument-name');
const TestGetPacketsAsync = require('./test-get-packets');

module.exports = {
    createConnection: require('./create-connection'),

    createClient: async function (connection, dumpSoapXml) {
        const client = await require('./client')();
        const check = require('./check')(dumpSoapXml);

        return {
            CancelRequestAsync: CancelRequestAsync(client, connection, check),
            CheckClientAuthenticationAsync: CheckClientAuthenticationAsync(client, connection, check),
            GetCachedInstrumentInfoReadOnlyAsync: GetCachedInstrumentInfoReadOnlyAsync(client, connection, check),
            GetCachedInstrumentInfoAsync: GetCachedInstrumentInfoAsync(client, connection, check),
            GetCachedLoggerInfoAsync: GetCachedLoggerInfoAsync(client, connection, check),
            GetLatestInstrumentFirmwareVersionAsync: GetLatestInstrumentFirmwareVersionAsync(client, connection, check),
            GetMyConnectionsEx2Async: GetMyConnectionsEx2Async(client, connection, check),
            GetPackets2Async: GetPackets2Async(client, connection, check),
            GetPacketsAsync: GetPacketsAsync(client, connection, check),
            RemoveAccessToInstrumentAsync: RemoveAccessToInstrumentAsync(client, connection, check),
            RemoveAccessToLoggerAsync: RemoveAccessToLoggerAsync(client, connection, check),
            RequestFullInstrumentInformation2Async: RequestFullInstrumentInformation2Async(client, connection, check),
            RequestReadAccessToInstrument2Async: RequestReadAccessToInstrument2Async(client, connection, check),
            RequestReadAccessToLoggerAsync: RequestReadAccessToLoggerAsync(client, connection, check),
            RequestReadOnlyKeyForConnectionAsync: RequestReadOnlyKeyForConnectionAsync(client, connection, check),
            RequestRetrieveInstrumentHistory2Async: RequestRetrieveInstrumentHistory2Async(client, connection, check),
            RequestWriteAccessToInstrument2Async: RequestWriteAccessToInstrument2Async(client, connection, check),
            RequestWriteAccessToLoggerAsync: RequestWriteAccessToLoggerAsync(client, connection, check),
            SetInstrumentNameAsync: SetInstrumentNameAsync(client, connection, check),
            TestGetPacketsAsync: TestGetPacketsAsync(client, connection, check)
        }
    }
}
