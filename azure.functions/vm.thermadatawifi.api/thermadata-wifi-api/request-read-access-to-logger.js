
const signature = require('./signature');

function fn(client, connection, check) {
    const RequestReadAccessToLoggerAsync = async args => {
        return await client.RequestReadAccessToLoggerAsync({
            connectionID: connection.id,
            serialNumber: args.serialNumber,
            readKey: args.readKey,
            authenticate: signature.generate(connection, [
                signature.fromString(args.serialNumber),
                signature.fromString(args.readKey)
            ])
        }).then(check);
    }
    RequestReadAccessToLoggerAsync.params = [`serialNumber`, `readKey`];
    return RequestReadAccessToLoggerAsync;
}

module.exports = fn;
