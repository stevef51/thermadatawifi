
const signature = require('./signature');

function fn(client, connection, check) {
    const GetCachedLoggerInfoAsync = async args => {
        return await client.GetCachedLoggerInfoAsync({
            connectionID: connection.id,
            serialNumber: args.serialNumber,
            authenticate: signature.generate(connection, [
                signature.fromString(args.serialNumber)
            ])
        }).then(check);
    }
    GetCachedLoggerInfoAsync.params = [`serialNumber`];
    return GetCachedLoggerInfoAsync;
}

module.exports = fn;


