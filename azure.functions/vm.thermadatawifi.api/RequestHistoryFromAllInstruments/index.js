const thermaDataWifiApi = require('../thermadata-wifi-api');
const getParam = require('../utils/get-param');
const _ = require('lodash');
const delay = require('delay');
const moment = require('moment');
const uuid = require('uuid');

module.exports = async function (context, req) {
    context.log('JavaScript HTTP trigger function processed a request.');

    try {
        let connection = {
            id: getParam(req, 'connectionId'),
            secret: getParam(req, 'connectionSecret')
        };
        let client = await thermaDataWifiApi(connection);

        let result = {
            instrumentCount: 0
        }
        let instruments = await client.GetMyConnectionsEx2Async();
        if (instruments) {
            let historyRequests = instruments.ConnectionsAvailableEx2
                .filter(i => i.ReadAccess)
                .map(i => client.RequestRetrieveInstrumentHistory2Async({
                    serialNumber: i.SerialNumber,
                    requestId: uuid().toString()
                }));
            result.instrumentCount = historyRequests.length;
            result.results = await Promise.all(historyRequests);
        }
        context.res = {
            body: result
        }
    }
    catch (e) {
        context.res = {
            status: 500,
            body: e.message
        }
    }
};