const thermaDataWifiApi = require('../thermadata-wifi-api');
const getParam = require('../utils/get-param');

module.exports = async function (context, req) {
    context.log('JavaScript HTTP trigger function processed a request.');

    try {
        let serialNumber = getParam(req, 'serialNumber');
        let readKey = getParam(req, 'readKey');

        let connection = {
            id: getParam(req, 'connectionId'),
            secret: getParam(req, 'connectionSecret')
        };
        let client = await thermaDataWifiApi(connection);
        context.res = {
            body: await client.RequestReadAccessToInstrument2Async({
                serialNumber,
                readKey
            })
        }
    }
    catch (e) {
        context.res = {
            status: 500,
            body: e.message
        }
    }
};