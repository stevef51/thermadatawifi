module.exports = function getParam(req, name, fnDefault) {
    let value = req.query[name];
    if (value == null) {
        if (fnDefault) {
            return fnDefault();
        }
        throw new Error(`Missing ${name}`);
    }
    return value;
}