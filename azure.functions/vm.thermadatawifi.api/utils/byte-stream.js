function ByteStream(buf) {
    this.buf = buf;
    this.index = 0;
}

ByteStream.prototype.more = function () {
    return this.index < this.buf.length;
}

ByteStream.prototype.next = function (count) {
    let data = this.buf.slice(this.index, count);
    this.index += count;
    return data;
}

ByteStream.prototype.UInt8 = function (count) {
    let result;
    if (count != null) {
        result = this.buf.slice(this.index, this.index + count);
        this.index += count;
    } else {
        result = this.buf.readUInt8(this.index++);
    }
    return result;
}

ByteStream.prototype.UInt16LE = function (count) {
    let result;
    if (count != null) {
        result = [];
        while (count--) {
            result.push(this.buf.readUInt16LE(this.index));
            this.index += 2;
        }
    } else {
        result = this.buf.readUInt16LE(this.index);
        this.index += 2;
    }
    return result;
}

ByteStream.prototype.UInt32LE = function (count) {
    let result;
    if (count != null) {
        result = [];
        while (count--) {
            result.push(this.buf.readUInt32LE(this.index));
            this.index += 4;
        }
    } else {
        result = this.buf.readUInt32LE(this.index);
        this.index += 4;
    }
    return result;
}

ByteStream.prototype.UInt32BE = function (count) {
    let result;
    if (count != null) {
        result = [];
        while (count--) {
            result.push(this.buf.readUInt32BE(this.index));
            this.index += 4;
        }
    } else {
        result = this.buf.readUInt32BE(this.index);
        this.index += 4;
    }
    return result;
}

ByteStream.prototype.UTF8String = function (count) {
    let b = this.buf.slice(this.index, this.index + count);
    let result = b.toString('utf8');
    this.index += count;
    return result;
}
module.exports = ByteStream;