const getParam = require('../utils/get-param');
const pollRequest = require('../logic/poll-packets');

module.exports = async function (context, req) {
    context.log('JavaScript HTTP trigger function processed a request.');

    const mongo = await require('../mongo')();
    const tenant = getParam(req, 'tenant', () => null);
    try {
        let tenants = [];

        const masterConfig = await mongo.getMasterConfig();
        if (tenant == null) {
            tenants = masterConfig.tenants;

        } else {
            tenants = masterConfig.tenants.filter(t == tenant);
        }

        let body = {};
        let promises = tenants.map(async tenant => {
            return pollRequest(tenant).then(result => {
                body[tenant] = result;
            })
        });

        await Promise.all(promises);

        context.res = {
            body
        }
    }
    catch (e) {
        context.res = {
            status: 500,
            body: e.message
        }
    }
};