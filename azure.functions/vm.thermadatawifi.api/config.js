const commonEnv = require('common-env')();

let config = commonEnv.getOrElseAll({
    VM: {
        mongoDb: {
            url: {
                $default: `mongodb://mongodb:27017`,
                $aliases: [`VM_MONGODB__URL`]
            },
            dbName: {
                $default: `virtualmgr`,
                $aliases: [`VM_MONGODB__DBNAME`]
            }
        },

        historyRetrievalMinutes: {
            $default: 6 * 60,
            $aliases: [`VM_HISTORYRETRIEVALMINUTES`]
        }
    }
});

console.log(`Config = ${JSON.stringify(config.VM, null, 2)}`);

module.exports = config.VM;