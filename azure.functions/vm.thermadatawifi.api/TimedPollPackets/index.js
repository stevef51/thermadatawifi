const pollRequest = require('../logic/poll-packets');

module.exports = async function (context, myTimer) {
    var timeStamp = new Date().toISOString();

    if (myTimer.IsPastDue) {
        context.log('JavaScript is running late!');
    }
    context.log('JavaScript timer trigger function ran!', timeStamp);

    const mongo = await require('../mongo')();
    try {
        const masterConfig = await mongo.getMasterConfig();
        const tenants = masterConfig.tenants;

        let body = {};
        let promises = tenants.map(async tenant => {
            return pollRequest(tenant).then(result => {
                body[tenant] = result;
            })
        });

        await Promise.all(promises);

        context.res = {
            body
        }
    }
    catch (e) {
        context.res = {
            status: 500,
            body: e.message
        }
    }

};