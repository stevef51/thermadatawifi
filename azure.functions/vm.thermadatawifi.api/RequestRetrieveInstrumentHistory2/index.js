const thermaDataWifiApi = require('../thermadata-wifi-api');
const uuid = require('uuid');
const getParam = require('../utils/get-param');

module.exports = async function (context, req) {
    context.log('JavaScript HTTP trigger function processed a request.');

    try {
        let serialNumber = getParam(req, 'serialNumber');
        let requestId = getParam(req, 'requestId', () => uuid().toString());

        let connection = {
            id: getParam(req, 'connectionId'),
            secret: getParam(req, 'connectionSecret')
        };
        let client = await thermaDataWifiApi(connection);
        context.res = {
            body: await client.RequestRetrieveInstrumentHistory2Async({
                serialNumber,
                requestId
            })
        }
    }
    catch (e) {
        context.res = {
            status: 500,
            body: e.message
        }
    }
};