const thermaDataWifiApi = require('../thermadata-wifi-api');
const _ = require('lodash');
const uuid = require('uuid');
const moment = require('moment');
const config = require('../config');

async function retrieveInstrumentHistoryForClient(tenantHostname, client) {
    const available = await client.GetMyConnectionsEx2Async({});
    if (available && available.ConnectionsAvailableEx2.length) {
        console.log(`${tenantHostname}: Requesting history for ${available.ConnectionsAvailableEx2.length} instruments ...`);
        let promises = available.ConnectionsAvailableEx2.map(cn => {
            return client.RequestRetrieveInstrumentHistory2Async({
                serialNumber: cn.SerialNumber,
                requestId: uuid.v4()
            })
        });
        await Promise.all(promises);
    } else {
        console.warn(`${tenantHostname}: Has 0 instruments`);
    }
}

module.exports = async function (tenantHostname) {
    const mongo = await require('../mongo')();
    const dbClient = await mongo.getTenantDb(tenantHostname);
    try {
        const pollSessionId = uuid().toString();
        let tenantConfig = await dbClient.getTenantConfig();
        if (tenantConfig == null) {
            tenantConfig = {
                connection: await thermaDataWifiApi.createConnection()
            }
            await dbClient.updateTenantConfig(tenantConfig);
            console.log(`${tenantHostname}: Polling tenant with NEW connectionId: ${tenantConfig.connection.id}, secret: ${tenantConfig.connection.secret} ...`);
        }
        else {
            console.log(`${tenantHostname}: Polling tenant with connectionId: ${tenantConfig.connection.id}, secret: ${tenantConfig.connection.secret} ...`);
        }
        const client = await thermaDataWifiApi.createClient(tenantConfig.connection);

        var nextHistoryRetrievalUtc = tenantConfig.lastHistoryRetrievalUtc ? moment(tenantConfig.lastHistoryRetrievalUtc).add(config.historyRetrievalMinutes, 'minutes') : null;
        if (nextHistoryRetrievalUtc == null || moment().isAfter(nextHistoryRetrievalUtc)) {
            tenantConfig.lastHistoryRetrievalUtc = moment().utc().toISOString();
            nextHistoryRetrievalUtc = moment(tenantConfig.lastHistoryRetrievalUtc).add(config.historyRetrievalMinutes, 'minutes');

            await retrieveInstrumentHistoryForClient(tenantHostname, client);
            await dbClient.updateTenantConfig(tenantConfig);
        }

        let packets = await client.GetPackets2Async({});
        let result = {
            lastHistoryRetrievalUtc: tenantConfig.lastHistoryRetrievalUtc,
            nextHistoryRetrievalUtc: nextHistoryRetrievalUtc.toISOString(),
            devices: {},
            pollSessionId,
            totalHistory: 0,
            totalBroadcast: 0
        }
        if (packets.Polls != null) {
            let readings = [];
            let probeDatas = [];
            _.forEach(packets.Polls, poll => {
                _.forEach(poll.packets, packet => {
                    switch (packet.type) {
                        case 'BroadcastReadingPacket':
                        case 'SavedReadingsPacket':
                            let lastDateTime = null;
                            _.forEach(packet.readings, r => {
                                r.serialNumber = poll.serialNumber;         // Add serialNumber to each reading
                                r.dateTime = new Date(r.dateTime.year, r.dateTime.month - 1, r.dateTime.day, r.dateTime.hour, r.dateTime.minute, r.dateTime.second);
                                lastDateTime = r.dateTime;
                                r.pollSessionId = pollSessionId;
                                readings.push(r);
                            })
                            if (packet.batteryLevel != null && lastDateTime != null) {
                                probeDatas.push({
                                    serialNumber: poll.serialNumber,
                                    battery: packet.batteryLevel,
                                    signalStrength: packet.signalStrength,
                                    dateTime: lastDateTime                      // The battery/signal strength dont have their own time stamp, so we use the last readings timestamp
                                })
                            }
                            let deviceResult = result.devices[poll.serialNumber];
                            if (deviceResult == null) {
                                deviceResult = result.devices[poll.serialNumber] = {};
                            }
                            if (packet.type === 'BroadcastReadingPacket') {
                                deviceResult.broadcast = (deviceResult.broadcast || 0) + packet.readings.length;
                                result.totalBroadcast += packet.readings.length;
                            } else {
                                deviceResult.history = (deviceResult.history || 0) + packet.readings.length;
                                result.totalHistory += packet.readings.length;
                            }
                            break;
                    }
                })
            })

            if (readings.length) {
                Object.assign(result, await dbClient.addReadings(readings));
                await dbClient.addProbeDatas(probeDatas);
            }
        }

        console.log(`${tenantHostname}: Polled ${Object.keys(result.devices).length} instruments, history: ${result.totalHistory}, broadcast: ${result.totalBroadcast}, inserted: ${result.inserted}, duplicates: ${result.duplicate}`);

        return result;
    } catch (err) {
        console.error(err);
        throw err;
    } finally {
        await mongo.close();
    }
}