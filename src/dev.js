// This is a command line utility to allow 'low-level' access to the Thermadata Wifi SOAP server
const program = require('commander');
const fs = require('fs');

program
    .option('--add-serial <serialNumber:readKey>')
    .option('--remove-serial <serialNumber>')
    .option('--get')
    .option(`--list`);

program.parse(process.argv);

function out(args) {
    console.log(JSON.stringify(args, null, 2));
}

async function main() {
    const connection = await require('./connection')();
    const api = await require('./thermadata-wifi-api')(connection);

    if (program.addSerial) {
        const nvp = program.addSerial.split(`:`);
        out(await api.RequestReadAccessToInstrument2Async({
            serialNumber: nvp[0],
            readKey: nvp[1]
        }));
    } else if (program.removeSerial) {
        out(await api.RemoveAccessToInstrumentAsync({
            serialNumber: program.removeSerial
        }));
    }

    if (program.get) {
        out(await api.GetPackets2Async({}));
    }

    if (program.list) {
        out(await api.GetMyConnectionsEx2Async());
    }
}

main();
