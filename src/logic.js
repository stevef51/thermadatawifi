const _ = require('lodash');

module.exports = async function () {
    const connection = await require('./connection')();
    const api = await require('./thermadata-wifi-api')(connection);

    let _instrumentSerialNumbers = Object.create(null);
    let _tenants = Object.create(null);

    async function refreshInstrumentsAsync() {
        let myInstruments = await api.GetMyConnectionsEx2Async();
        if (myInstruments != null) {
            _instrumentSerialNumbers = _.chain(myInstruments.ConnectionsAvailableEx2)
                .map(c => [`D${c.SerialNumber}`, c])            // API returns serialNumbers with 'D' stripped, add it back for Id purposes
                .fromPairs()
                .value();
        } else {
            _instrumentSerialNumbers = Object.create(null);
        }

        let removeInstruments = _.clone(_instrumentSerialNumbers);
        _.map(_tenants, tenant => {
            _.map(tenant.instruments, tenantInstrument => {
                const myInstrument = _instrumentSerialNumbers[tenantInstrument.serialNumber];
                tenantInstrument.information = myInstrument;
                delete removeInstruments[tenantInstrument.serialNumber];
            });
        });

        await Promise.all(_.map(removeInstruments, async (__, serialNumber) => {
            await api.RemoveAccessToInstrumentAsync({
                serialNumber
            });
        }));
    }

    async function syncTenantsAsync() {
        let refreshRequired = false;

        await refreshInstrumentsAsync();

        await Promise.all(_.map(_tenants, async tenant => {
            return await Promise.all(_.map(tenant.instruments, async instrument => {
                if (_instrumentSerialNumbers[instrument.serialNumber] == null) {
                    let result = await api.RequestReadAccessToInstrument2Async({
                        serialNumber: instrument.serialNumber,
                        readKey: instrument.readKey
                    });
                    if (result == 'Success') {
                        refreshRequired = true;
                    }
                }
            }));
        }));

        if (refreshRequired) {
            await refreshInstrumentsAsync();
        }
    }

    return {
        registerTenantAsync: async function (tenant) {
            _tenants[tenant.id] = tenant;
        },

        syncTenantsAsync: async function () {
            await syncTenantsAsync();

            return _tenants;
        },

        pollAsync: async function () {
            let results = Object.create(null);
            for (var tenant in _tenants) {
                results[tenant.id] = syncTenant(tenant);
            }
        },

        listAsync: async function () {
            return await api.GetMyConnectionsEx2Async();
        }
    }
}