const fs = require('fs');
const thermaDataWifi = require('../azure.functions/vm.thermadatawifi.api/thermadata-wifi-api');

// This will create a new connection if one does not already exist
async function getConnectionAsync(force) {
    let connection = {};
    if (!force && fs.existsSync('./connection.json')) {
        connection = JSON.parse(fs.readFileSync('./connection.json'));
        console.error(`Using existing Connection: id ${connection.id} secret ${connection.secret} ..`);
    } else {
        connection = await thermaDataWifi.createConnection();
        console.error(`Created new Connection: id ${connection.id} secret ${connection.secret} ..`);
        fs.writeFileSync('./connection.json', JSON.stringify(connection, null, 2));
    }
    return connection;
}

module.exports = getConnectionAsync
