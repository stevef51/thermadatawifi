const program = require('commander');
const fs = require('fs');

program
    .option(`-i, --input <string>`)

program.parse(process.argv);

async function main() {
    const logic = await require('./logic')();

    if (program.input) {
        const tenantsConfig = fs.readFileSync(program.input);

        await logic.registerTenantAsync(tenantsConfig);
        console.log(JSON.stringify(await logic.syncTenantsAsync()));
    }
}

main();
