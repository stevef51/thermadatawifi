const _ = require('lodash');

// This is a command line utility to allow 'raw-level' access to the Thermadata Wifi SOAP server
function out(args) {
    console.log(JSON.stringify(args, null, 2));
}

function makeDescriptor(funcName, args) {
    let descriptor = {};
    descriptor[funcName] = args;
    return descriptor;
}

async function main(commands) {
    let dumpSoap = commands.indexOf('--dumpSoap');
    if (dumpSoap >= 0) {
        commands.splice(dumpSoap, 1);
    }
    dumpSoap = dumpSoap >= 0;

    const connection = await require('./connection')();
    const api = await require('../azure.functions/vm.thermadatawifi.api/thermadata-wifi-api').createClient(connection, dumpSoap);

    let help = commands.indexOf('--help');
    if (help >= 0) {
        commands.splice(help, 1);
    }
    help = help >= 0;       // Convert to found bool

    let descriptors = [];
    let i = 0;
    while (i < commands.length) {
        const method = commands[i++];

        const func = api[`${method}Async`];
        if (func) {
            let methodArgs = Object.create(null);
            let j = 0;
            while (j < func.params.length) {
                methodArgs[func.params[j++]] = i < commands.length ? commands[i++] : null;
            }
            descriptors.push(makeDescriptor(method, methodArgs));
        }
    }

    if (help) {
        if (descriptors.length == 0) {
            descriptors = _.map(api, (func, method) => makeDescriptor(method.replace('Async', ''), func.params));
        }
        out(descriptors);
    } else {
        await Promise.all(_.map(descriptors, async descriptor => {
            const methodName = Object.keys(descriptor)[0];
            const func = api[`${methodName}Async`];
            const result = await func(descriptor[methodName]);
            out(result);
        }))
    }
}

main(process.argv.slice(2));
